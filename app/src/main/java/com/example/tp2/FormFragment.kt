package com.example.tp2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class FormFragment : Fragment() {

   override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val btnSave = view.findViewById<Button>(R.id.buttonSave)
        val etName = view.findViewById<EditText>(R.id.etName)
        val etHobby = view.findViewById<EditText>(R.id.etHobby)

        btnSave.setOnClickListener {
            val name = etName.text.toString()
            val hobby = etHobby.text.toString()

            // Update the app bar with the username
            updateUsernameInAppBar(name)

            // Clear the input fields
            etName.text.clear()
            etHobby.text.clear()
        }


    }

    private fun updateUsernameInAppBar(username: String) {
        val activity = activity as AppCompatActivity
        //activity.supportActionBar?.title = "Hello, $username"

        val actionBar = activity.supportActionBar
        actionBar?.setCustomView(R.layout.actionbar_custom_layout)
        val customTitle = actionBar?.customView?.findViewById<TextView>(R.id.action_bar_title)
        customTitle?.text =  "Hello, $username"
    }

}